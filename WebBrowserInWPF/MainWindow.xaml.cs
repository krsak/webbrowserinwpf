﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WebBrowserInWPF
{
	/// <summary>
	/// MainWindow.xaml の相互作用ロジック
	/// </summary>
	public partial class MainWindow : Window
	{
		[System.Runtime.InteropServices.ComVisible(true)]
		public class MyObject
		{
			public string MyFunc(int num)
			{
				MessageBox.Show(string.Format("C# {0}", num));
				return string.Format("{0}#{0}", num);
			}
		}

		private MyObject _MyObject = new MyObject();

		public MainWindow()
		{
			InitializeComponent();

			webBrowser.ObjectForScripting = this._MyObject;

			this.OpenHtmlFile();
		}

		public async void OpenHtmlFile()
		{
			var exe_dir = AppDomain.CurrentDomain.BaseDirectory;
			using (var sr = new StreamReader(System.IO.Path.Combine(exe_dir, "html", "mypage.html"))) {
				var buf = await sr.ReadToEndAsync();
				webBrowser.NavigateToString(buf);
			}
		}

		private void button1_Click(object sender, RoutedEventArgs e)
		{
			// 「メソッド C#」ボタン
			// WPFから、C#のメソッドを呼び出す
			var ret = this._MyObject.MyFunc(9876);
		}

		private void button2_Click(object sender, RoutedEventArgs e)
		{
			// 「メソッド JavaScript」ボタン
			// WPFから、JavaScriptのメソッドを呼び出す
			var ret = webBrowser.InvokeScript("MyFunc", new[] { 8765.ToString() });
		}
	}
}
